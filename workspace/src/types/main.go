package main

import "fmt"

type foo func()

type MeineMap map[string]float32

type MeinInt int

func (mi MeinInt) Print() {
	fmt.Println("Print:", mi)
}

func (mi *MeinInt) Modify() {
	mi.Print()
	*mi = 433
}

func (f foo) CallMe() {
	fmt.Println("CallMe")
	f()
}

func hurra() {
	fmt.Println("Hurra")
}

func NewMeinint() *MeinInt {
	var mi MeinInt
	//	mi = 788
	return &mi
}

func main() {

	var fn foo

	fn = foo(hurra)

	fn()

	fn.CallMe()

	var mi MeinInt

	mi = 10

	var i int = 42

	mi = MeinInt(i)

	mi *= 10

	fmt.Println(mi)
	mi.Print()
	//(&mi).Modify()
	mi.Modify()
	fmt.Println(mi)
}
