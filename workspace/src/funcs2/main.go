package main

import "fmt"

func hallo(fn func()) {

	if fn != nil {
		fn()
	}
}

func works() {
	fmt.Println("hello from works")
}

func countTo(n int) func() (int, bool) {
	i := 0
	return func() (int, bool) {
		x := i
		i++
		return x, x < n
	}
}

func counter(n int) func() int {
	n--
	return func() int {
		n++
		return n
	}
}

func main() {

	up := countTo(3)
	for n, ok := up(); ok; n, ok = up() {
		fmt.Printf("counter: %d\n", n)
	}

	var fn func()

	hallo(fn)

	fn = works

	hallo(fn)
	hallo(works)

	var x int = 42

	fn = func() {
		fmt.Println("From inner function", x)
		x++
	}

	fn()
	hallo(fn)
	hallo(func() { fmt.Println("From inner function 2") })

	c := counter(10)
	fmt.Println(c())
	fmt.Println(c())
	fmt.Println(c())
}
