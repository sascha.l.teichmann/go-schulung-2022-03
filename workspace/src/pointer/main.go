package main

import "fmt"

func modify2(x *int) {
	y := *x
	*x *= 2

	y = 7777

	_ = y
}

func modify(x *int) {
	*x = 42
	modify2(x)
}

func create() *int {
	var x int
	return &x
}

func addData(x *[]int) {
	*x = append(*x, 1, 2, 3, 4, 5)
}

func main() {

	p := new(int)

	// var data int
	// p1 := &data

	fmt.Println(*p, p)

	var y = 32
	modify(&y)
	fmt.Println(y)

	data := make([]int, 0, 20)

	addData(&data)
	fmt.Println("data:", data)
}
