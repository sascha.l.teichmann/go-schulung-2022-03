package main

import "fmt"

type Adresse struct {
	ort     string
	strasse string
}

type Konstante struct {
	next *Konstante

	adr Adresse

	Pi   float64 // `json:"pi"`
	_    byte
	name string
	_    [4]byte
}

func handle(st *Konstante) {
	st.name = "Hurra"
}

func main() {

	var st = Konstante{
		Pi: 3.1415,
		adr: Adresse{
			ort:     "Dorf",
			strasse: "weg",
		},
	}

	st.name = "Kreiszahl"

	fmt.Println(st)
	handle(&st)
	fmt.Println(st)
}
