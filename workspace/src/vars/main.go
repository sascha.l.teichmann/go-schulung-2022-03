package main

func main() {
	/*
		var x int  // plattform abh. mind 32bit. -> 64bit
		var y uint // vorzeichenlos.

		// int8, int16, int32, int64
		// uint8, uint16, uint32, uint64

		// uint8, u...
		// byte

		// float32, float64

		// bool

		cond := true
		// var cond bool = true
		// var cond bool = false

		//var v complex64
		// var v complex128

		var c rune // 32 bit int (int32) -> Unicode-Code-Points.

		c = 'A'
		c = '\u8989'

		var s string

		empty := s == ""

		// uintptr // plattform

		var b byte
		// var b uint8

		_ = x

		var v int64
		var w int32

		// v = w

		v = int64(w)

		var x int = 89
		var b bool

			if x != 0 {
				b = true
			} else {
				b = false
			}
		b := x != 0

		b := true
		var x int

		// x = b ? 1 : 0
		if b {
			x = 1
		} else {
			x = 0
		}

		var x float64
		var i int

		x = float64(i)

		var pi float64 = 3
		var x int = 1e10
	*/

	test := 1  // int
	test = 1.0 // float64
	//test := 3.2 // float64

	//test := float32(3.2)

	_ = test

}
