package main

import (
	"fmt"
	"sort"
)

type sortableInts []int

func (s sortableInts) Len() int           { return len(s) }
func (s sortableInts) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s sortableInts) Less(i, j int) bool { return s[i] < s[j] }

func main() {
	orig := []int{2, 4, 6, 1, 3, 5}

	a := make([]int, len(orig))
	copy(a, orig)

	fmt.Println(orig)

	sort.Ints(a)
	//a = bubblesort(int, intLess, a)
	fmt.Println(a)

	copy(a, orig)
	sort.Slice(a, func(i, j int) bool {
		return a[i] < a[j]
	})

	copy(a, orig)
	sort.Sort(sortableInts(a))
	fmt.Println(a)

	fmt.Println(a)
}
