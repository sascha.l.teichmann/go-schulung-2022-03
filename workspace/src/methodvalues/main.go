package main

import "fmt"

type A int

func (a A) print() {
	fmt.Println("a print", a)
}

func execute(fn func()) {
	fmt.Println("execute")
	fn()
}

func doIt() {
	fmt.Println("doit")
}

func main() {

	var a A = 42
	a.print()

	execute(doIt)

	fn := a.print // --> func()
	execute(fn)

	execute(func() { a.print() })
}
