package main

import "fmt"

type Base int

type Derived Base

type ZweiteBase int

type Embed struct {
	Base
	ZweiteBase
}

func (zw ZweiteBase) ZweiteBasis() {
	fmt.Println("ZweiteBasis: ZweiteBase")
}

func (b Base) BasisMethode() {
	fmt.Println("BasisMethode")
}

func (b Base) ZweiteBasis() {
	fmt.Println("ZweiteBasis: Base")
}

func (d Derived) DerivedMethod() {
	fmt.Println("DerivedMethod")
}

func (e *Embed) ZweiteBasis() {
	fmt.Println("------")
	e.Base.ZweiteBasis()
	e.ZweiteBase.ZweiteBasis()

	e.Base = 32
}

func main() {

	var b Base
	b.BasisMethode()

	var d Derived
	//d.BasisMethode()
	d += 10
	d.DerivedMethod()

	var e Embed

	e.BasisMethode()
	e.ZweiteBasis()

	fmt.Println("After call:", e.Base)

}
