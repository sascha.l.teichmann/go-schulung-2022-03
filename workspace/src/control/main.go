package main

import "fmt"

func main() {

	if cond := 2 < 1; cond {
		fmt.Println("true", cond)
	} else if false {
		// ...
	} else {
		fmt.Println("false", cond)
	}

	//do {
	//} while (cond)

	// for {
	//    if !cond {
	//       break
	//    }
	//}

outer:
	for j := 0; j < 3; j++ {
		//inner:
		for i := 0; i < 6; i++ {
			fmt.Println(j, i)

			if i == 4 {
				break outer
			}

			fmt.Println("after:", i)
		}
		//goto inner
	}

	//goto inner

	switch value := "Jürgen"; value {
	case "Klaus", "Otto":
		fmt.Println("Hurra")
	case "Peter":
		fmt.Println("Buh")
	case "Rita":
		fmt.Println("Naja")

	default:
		fmt.Println("so so")
	}

	a := 15
	switch {
	case 0 <= a && a <= 10:
		fmt.Println("a zwischen 0 und 10")
		fallthrough
	case 11 <= a && a <= 20:
		fmt.Println("a zwischen 20 und 20")
	default:
		fmt.Println("zu gross")

	}

}
