package main

import "fmt"

type Auto interface {
	Anfahren()
	Speed() float32
	Anhalten()
}

func fahren(a Auto) {
	a.Anfahren()
	fmt.Printf("Speed: %f\n", a.Speed())
	a.Anhalten()
}

type Komplett float32

func (k Komplett) Anfahren() {
	fmt.Println("Komplett: Anfahren")
}

func (k Komplett) Speed() float32 {
	return float32(k)
}

func (k Komplett) Anhalten() {
	fmt.Println("Komplett: Anhalten")
}

type Motor int

type Bremse rune

type Tacho float32

func (m Motor) Anfahren() {
	fmt.Println("Motor: Brummm!")
}

func (b Bremse) Anhalten() {
	fmt.Println("Bremse: Quietsch")
}

func (t Tacho) Speed() float32 {
	return float32(t)
}

func main() {

	k := Komplett(120)

	fahren(k)

	fahren(struct {
		Motor
		Bremse
		Tacho
	}{Tacho: 200})
}
