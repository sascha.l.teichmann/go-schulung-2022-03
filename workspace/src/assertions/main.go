package main

import (
	"fmt"
	"image"
)

type Printer interface {
	Print()
}

type Implementation complex64

func (i Implementation) Print() {
	fmt.Println("impl:", i)
}

func subImage(
	img image.Image,
	rect image.Rectangle,
) (image.Image, bool) {

	if i, ok := img.(interface {
		SubImage(r image.Rectangle) image.Image
	}); ok {
		return i.SubImage(rect), true
	}
	return nil, false
}

func myPrint(args ...any) {

	for _, arg := range args {

		// x := arg.(int)

		if x, ok := arg.(int); ok {
			fmt.Println("Eins", x, ok)
		}

		switch x := arg.(type) {
		case int, string:
			fmt.Printf("mixed: %T\n", x)
		case float64:
			fmt.Println("float64", x)
		case Printer:
			x.Print()
		default:
			fmt.Println("unknown", x)
		}

	}
}

func main() {
	//myPrint(1, 31.167, "hallo")

	var c Implementation
	myPrint(1)
	myPrint(1.344)
	myPrint("Hello")
	myPrint(c)
}
