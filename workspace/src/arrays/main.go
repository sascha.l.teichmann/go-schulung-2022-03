package main

import "fmt"

func sum(arr [10]int) int {
	s := 0
	//fmt.Println("len:", len(arr))
	//for i := 0; i < len(arr); i++ {
	//fmt.Println(":::", arr[i])
	//}
	for _, v := range arr {
		s += v
	}
	x := 10
	arr[x] = 43
	return s
}

func main() {

	arr := [10]int{
		3: 0, 5: 2, 2: 4, 7: 5, 1: 6,
	}

	arr[3] = 42

	fmt.Println(arr)
	fmt.Println(sum(arr))
}
