package main

import (
	"flag"
	"fmt"
	"log"
)

type myError int

func (me myError) Error() string {
	return fmt.Sprintf("Fehler: %d", me)
}

func working(input int) (int, error) {

	// var f float32
	// f = float32(input)

	if input == 5 {
		//return 0, errors.New("es ist eine 5")
		return 0, myError(5)
	}

	return input*3 + 10, nil
}

func main() {
	//fmt.Println(working(32))

	input := flag.Int("input", 42, "Programm Input")
	flag.Parse()

	fmt.Println("b")

	var err error

	println := func(args ...any) {
		if err == nil {
			_, err = fmt.Println(args...)
		}
	}

	println("b")
	println("b")
	println("b")
	println("b")

	if err != nil {
		// ...
	}

	// Err-lang
	if x, err := working(*input); err != nil {
		if _, ok := err.(myError); ok {
			fmt.Println("Selber schuld!")
		}

		log.Fatalf("error: %v\n", err)
	} else {
		fmt.Println(x)
	}
}
