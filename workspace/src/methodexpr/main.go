package main

import (
	"fmt"

	typ "github.com/go-pg/pg/types"
)

type B float32

func (b B) erste() { // func erste(b B) {
	fmt.Println("1")
}

func (b B) zweite() {
	fmt.Println("2")
}

func (b B) dritte() {
	fmt.Println("3")
}

func execute(fn func(B)) {

	var b B
	fn(b)
}

func main() {

	execute(B.erste)
	execute(B.zweite)
	execute(B.dritte)

	x := typ.Array{}

	_ = x
}
