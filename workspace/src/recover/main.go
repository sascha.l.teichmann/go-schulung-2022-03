package main

import (
	"fmt"
	"log"
	"os"
)

func powerfulCalculation(input int) {

	if input == 10 {
		//return
		panic("kkkk")
		// falsch
	}

	powerfulCalculation(input + 1)
}

func API() (p error) {

	f, err := os.Open("main.go")
	if err != nil {
		return err
	}

	defer func() {
		fmt.Println("schliessen")
		f.Close()
	}()

	// ....

	defer func() {
		fmt.Println("defer")
	}()

	defer func() {
		if x := recover(); x != nil {
			fmt.Println(x)
			p = fmt.Errorf("panic gefangen: %v", x)
		}
	}()

	powerfulCalculation(0)

	x := recover()
	fmt.Println("recover:", x)

	return nil
}

func main() {
	if err := API(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
	fmt.Println("Shiny!")
}
