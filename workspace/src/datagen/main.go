package main

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

type Tree[K constraints.Ordered, V any] struct {
	left  *Tree[K, V]
	right *Tree[K, V]
	key   K
	value V
}

func (t *Tree[K, V]) find(key K) V {
	if t.key == key {
		return t.value
	}
	var empty V
	return empty
}

func main() {

	tint := Tree[int, string]{key: 42, value: "Hallo"}

	var y int

	y = tint.key

	fmt.Println(tint, y)

	fmt.Println(tint.find(42))
}
