package main

import (
	"fmt"
)

func main() {

	// var ch chan bool
	done := make(chan struct{})

	mesg := make(chan string)

	go func() {
		defer func() { done <- struct{}{} }()
		//defer close(done)

		for m := range mesg {
			fmt.Println(m)
		}

	}()

	mesg <- "Hallo aus der Go-Routine"
	mesg <- "Meine zweite nachricht"
	fmt.Println("last message sent")

	close(mesg)

	fmt.Println("Hallo")

	_, ok := <-done
	fmt.Println("ok:", ok)

	// time.Sleep(time.Second)
}
