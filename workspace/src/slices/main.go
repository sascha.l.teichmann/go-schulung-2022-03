package main

import "fmt"

func modify(sl []int) {
	sl[0] = 677
}

func main() {
	var arr [10]int

	arr[1] = 42

	//sl2 := arr[:]

	var sl []int

	fmt.Println(arr)
	fmt.Println(sl, sl == nil, len(sl))

	sl = arr[1 : 2+1]

	//0 1 2 3 4 5 ... 9 // arr

	// 0 1  // sl

	sl = sl[:len(sl)+1]
	fmt.Println("cap:", cap(sl))
	fmt.Println(sl, sl == nil, len(sl))
	modify(sl)
	fmt.Println(arr[1])

	ds := make([]int, 0, 2)

	fmt.Println(len(ds))
	fmt.Println(cap(ds))

	ds = append(ds, 43)
	fmt.Println(len(ds))
	ds = append(ds, 43)
	fmt.Println(len(ds))
	ds = append(ds, 43)
	fmt.Println(len(ds))

}
