package main

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

type Minable interface {
	~int | float64 | string
}

type myInt int

//func min[T constraints.Ordered](a, b T) T {
func min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}
func main() {

	a, b := myInt(1), myInt(-3)

	mm := min(a, b)
	fmt.Println(mm)

	//mo := min[int](-1, 67)
	//mo := min(-1, 67)
	mo := min[float64](-1.5, 67)
	fmt.Println(mo)

	m1 := min(1.3, 1.4)
	fmt.Println(m1)
	m2 := min(1, 3)
	fmt.Println(m2)
}
