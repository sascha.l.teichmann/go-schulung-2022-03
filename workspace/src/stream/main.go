package main

import (
	"io"
	"os"
)

type AUpper struct {
	io.Reader
}

func (au *AUpper) Read(b []byte) (n int, err error) {
	n, err = au.Reader.Read(b)
	for i, x := range b[:n] {
		if x == 'A' {
			b[i] = 'a'
		}
	}
	return n, err
}

func main() {
	io.Copy(os.Stdout, &AUpper{os.Stdin})
}
