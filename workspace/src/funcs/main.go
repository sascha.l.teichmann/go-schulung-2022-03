package main

import (
	"fmt"
)

func hallo(s, t string, n int) (m string, k int) { // value value übergabe
	s = "Moon"

	for i := 0; i < n; i++ {
		m += s + t
	}
	//i = 10
	return
}

func main() {

	//os.Args

	x := "Welt"
	y, _ := hallo("Hallo", x, 10)

	x, y = y, x

	fmt.Println(y)
	//os.Exit(1)
}
