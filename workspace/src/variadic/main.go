package main

import "fmt"

func myprint(greet string, args ...int) {
	for _, arg := range args {
		fmt.Printf("%s: %d\n", greet, arg)
	}
}

func main() {

	args := []int{42, 67, 69}

	fmt.Println("vim-go", 2, 34.12)
	myprint("a")
	myprint("b", 1, 2)
	myprint("c", 1, 2, 3)

	myprint("d", args...)

	xyz := append([]int(nil), args...)
	//xyz := append(nil, args...)

	args[0]++

	fmt.Println(xyz)
	fmt.Println(args)
}
