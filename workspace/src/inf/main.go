package main

import (
	"fmt"
)

func bad() {
	panic("kk")
}

func good() {
	defer func() {
		recover()
		fmt.Println("You understand")
	}()
	bad()
}

func main() {

	good()
}
