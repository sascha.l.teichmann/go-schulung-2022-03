package main

import (
	"regexp"
)

var expr = regexp.MustCompile("(aaa)")

/*

var expr *regexp.Regexp

func init() {
	expr = regexp.MustCompile("(aaa)")
}

*/

func foo() {
	//panic("PANIC!")

	userinput := "(bbb)"

	r1, err := regexp.Compile(userinput)
	if err != nil {
	}

	_ = r1

	/*

		a := []int{1, 2}

		fmt.Println(a[2])
	*/

}

func main() {
	foo()
}
