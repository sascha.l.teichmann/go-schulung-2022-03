package hello

import (
	"fmt"
)

var local int

var Global int = 53

func init() {
	fmt.Println("init from hello")
}

func init() {
	fmt.Println("init from hello 9")
}

func Greet() {
	//pq.Efatal()
	//x := hstore.Hstore{}
	fmt.Println("greet from hello")
	//Efatal()
	local = 42
}
