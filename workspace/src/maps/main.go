package main

import (
	"fmt"
	"sort"
)

func main() {

	m := map[string]int{
		"Jürgen": 89,
		"Rosi":   0,
	}

	// m := map[string]int{}
	// m := make(map[string]int)

	fmt.Println(m, m == nil, len(m))

	//m = make(map[string]int)

	m["Otto"] = 67

	fmt.Println(m, m == nil, len(m))

	fmt.Println(m["Jürgen"])
	fmt.Println(m["Rita"])
	fmt.Println(m["Rosi"])

	if v, ok := m["Rita"]; ok {
		fmt.Printf("Rita hat %d Punkte.\n", v)
	}

	if v, ok := m["Rosi"]; ok {
		fmt.Printf("Rosi hat %d Punkte.\n", v)
	}

	for k, v := range m {
		fmt.Println("~", k, v)
	}

	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		fmt.Printf("%s: %d\n", k, m[k])
	}

	//hashes := map[[]byte]int64{}
	hashes := map[string]int64{}

	pseudo := []byte{1, 2, 9}

	hashes[string(pseudo)] = 56

}
