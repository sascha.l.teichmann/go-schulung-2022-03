package main

import (
	"fmt"
	"meinemodule/hello"
)

func Hallo() {
	//int x;
	var x int = 42
	//var x int = 23

	//var int float32

	//_ = x

	//var y int = 2 * x

	y := 2 * x

	//x = 42
	{
		x := 3.14
		fmt.Println(x)
	}

	fmt.Println(x, y)
}

func main() {
	Hallo()
	hello.Greet()
	fmt.Println("Hello")
	hello.Global = 75
	fmt.Println(hello.Global)
}
